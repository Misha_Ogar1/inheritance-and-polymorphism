package Bank;
public class BankAccount {

    //переменная moneyInput
    int moneyInput = 0;

    //переменная moneyOut
    int moneyOut = 0;

    //переменная sum
    int sum = 0;

    //метод для внесения денег на счёт
    public void inputMoney(int moneyInput) {
        this.moneyInput = moneyInput;
        sum += moneyInput;
    }

    //метод для снятия денег со счёта
    public void outMoney(int moneyOut) {
        this.moneyOut = moneyOut;
        sum -= moneyOut;
    }

    //метод для вывода остатка на счёте
    public void listCheck() {
        System.out.println(sum);
    }
}
