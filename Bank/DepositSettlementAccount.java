package Bank;

public class DepositSettlementAccount extends BankAccount {
    //изначальное время
    long timeStart = System.nanoTime();


    //метод для внесения денег на счёт
    public void inputMoney(int moneyInput) {
        //изначальное время
        timeStart = System.nanoTime();
        sum = moneyInput;
    }

    //метод для снятия денег со счёта
    public void outMoney(int moneyOut) {
        //конечное время
        long timeEnd = System.nanoTime();
        System.out.println("timeStart: " + timeStart + " timeEnd: " + timeEnd + " timeEnd - timeStart: " + (timeEnd - timeStart));
        //если прошло 30 дней с момента последнего пополнения счёта, то снимаем деньги
        if ((timeEnd - timeStart) >= 1000000000 * 86400 * 30 ) {
            sum -= moneyOut;
        }
    }
}
