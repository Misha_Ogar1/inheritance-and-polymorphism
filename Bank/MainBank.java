package Bank;

public class MainBank {

    //метод main
    public static void main(String[] args) {

        //создание экземпляра класса BankAccount
        BankAccount bankAccount = new BankAccount();

        bankAccount.inputMoney(1000);

        bankAccount.outMoney(100);

        System.out.println("Вывод обычного счёта: ");
        bankAccount.listCheck();

//        //создание объекта CardAccount
        CardAccount cardAccount = new CardAccount();

        //положим деньги на CardAccount
        cardAccount.inputMoney(1000);

        //снятие денег с CardAccount
        cardAccount.outMoney(100);

        System.out.println("Вывод карточного счёта: ");
        cardAccount.listCheck();

        //создание объекта DepositSettlementAccount
        DepositSettlementAccount depositSettlementAccount = new DepositSettlementAccount();

        //положим деньги на depositSettlementAccount
        depositSettlementAccount.inputMoney(10000);

        //снятие денег с depositSettlementAccount
        depositSettlementAccount.outMoney(5000);

        System.out.println("Вывод денег с депозитного счёта: ");
        depositSettlementAccount.listCheck();

    }
}
