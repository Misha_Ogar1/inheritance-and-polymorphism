package Bank;

public class CardAccount extends BankAccount {

    //метод для внесения денег на счёт
    public void inputMoney(int moneyInput) {
        this.moneyInput = moneyInput;
        sum += moneyInput;
    }

    //метод для снятия денег со счёта без 1%
    public void outMoney(int moneyOut) {
        this.moneyOut = moneyOut;
        int percent = moneyOut / 100;
        sum = sum - moneyOut - percent;
    }
}
