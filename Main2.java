import figures.Circle;
import figures.Figure;
import figures.Square;

import java.util.ArrayList;
import java.util.TreeSet;

public class Main2 {

    //метод Main
    public static void main(String[] args) {
//
//        Figure figure = getNextFigure();
//        figure.getVisibleHeight();
//    }
//
//    //метод для получения фигуры
//    public static Figure getNextFigure() {
//        return new Circle(67);

//        //полиморфизм
//        //создание списка
//        ArrayList<Figure> squares = new ArrayList<>();
//        squares.add(new Circle(10.3));
//        squares.add(new Square(50));
//
//        //выводим значения параметров созданных объектов
//        for (Figure figure : squares) {
//            System.out.println(figure.getVisibleHeight());
//        }


        //создание коллекцию, содержащую квадраты
        TreeSet<Square> squares = new TreeSet<>();
        squares.add(new Square(40));
        squares.add(new Square(60));
        squares.add(new Square(10));
        squares.add(new Square(20));
        squares.add(new Square(20));

        //вводим значения ширины созданных квадратов
        for (Square square : squares) {
            System.out.println(square.getWidth());
        }
    }
}

