package ClientBank;

public class MainClient {

    //метод main
    public static void main(String[] args) {

        //создание объекта физического лица
        Individuals individuals = new Individuals();
        individuals.inputMoney(100);
        individuals.outMoney(30);
        System.out.println("Счёт физического лица: ");
        individuals.listCheck();
        System.out.print(System.lineSeparator());

        //создание объекта юридического лица
        LegalEntities legalEntities = new LegalEntities();
        legalEntities.inputMoney(500);
        legalEntities.outMoney(100);
        System.out.println("Счёт юридического лица: ");
        legalEntities.listCheck();
        System.out.print(System.lineSeparator());

        //создание объекта индивидуального предпринимателя
        IndividualEntrepreneurs individualEntrepreneurs = new IndividualEntrepreneurs();
        individualEntrepreneurs.inputMoney(100);
        System.out.println("Счёт ИП после вложения до 1000 руб: ");
        individualEntrepreneurs.listCheck();
        System.out.println("Счёт ИП после вложения суммы, превышающей или равной 1000 руб: ");
        individualEntrepreneurs.inputMoney(10_000);
        individualEntrepreneurs.listCheck();

    }
}
