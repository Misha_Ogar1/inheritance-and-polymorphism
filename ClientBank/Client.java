package ClientBank;

public abstract class Client {

    //счёт
    double check;

    //положить деньги на счёт
    public void inputMoney(double inputMoney) {
        check += inputMoney;
    }

    //снять деньги со счёта
    public void outMoney(double outMoney) {
        check -= outMoney;
    }

    //показать счёт
    public void listCheck() {
        System.out.println(check);
    }
}
