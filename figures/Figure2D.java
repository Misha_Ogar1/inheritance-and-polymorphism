package figures;

import java.awt.*;

//использование abstract, для того, чтобы нельзя было создавать объекты класса Figure2D
public abstract class Figure2D implements Figure {

    //создание переменной цвет, класса Color
    private Color color;

    //геттер, возвращающий цвет
    public Color getColor() {
        return color;
    }

    //сеттер для ввода цвета
    public void setColor(Color color) {
        this.color = color;
    }

    //обязуем все классы реализовать метод вычисления площади
    public abstract double getSquare();
}
