package figures;

import java.util.Date;

public class Rectangle extends Figure2D {

    //ширина
    private double width;

    //высота
    private double height;

    //создание переменной для времени
    private Date creationTime;

    //конструктор, для задания ширины и длины прямоугольника
    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
        setCreationTime();
    }

    //геттер для получения ширины
    public double getWidth() {
        return width;
    }

    //сеттер для задания ширины
    public void setWidth(double width) {
        this.width = width;
    }

    //геттер для получения высоты
    public double getHeight() {
        return height;
    }

    //сеттер для задания высоты
    public void setHeight(double height) {
        this.height = height;
    }

    //метод для вычисления площади
    public double getSquare() {
        return width * height;
    }

    //сеттер для установки текущих даты и времени
    private void setCreationTime() {
        creationTime = new Date();
    }

    //геттер для вывода текущих даты и времени
    public Date getCreationTime() {
        return creationTime;
    }

    @Override
    //получить видимую ширину
    public double getVisibleWeigth() {
        return 0;
    }

    @Override
    //получить видимую высоту
    public double getVisibleHeight() {
        return 0;
    }
}
