package figures;

public abstract class Figure3D implements Figure {

    //получить объём фигуры
    public abstract double getVolum();

    //получить площадь поверхности
    public abstract double getSurfaceSquare();
}
