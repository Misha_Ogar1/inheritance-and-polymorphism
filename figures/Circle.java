package figures;

public class Circle extends Figure2D {

    //создание переменной радиус
    public double radius;

    //создание конструктора Circle
    public Circle(double radius) {
        this.radius = radius;
    }

    //вычисляем и возвращаем площадь круга
    @Override
    public double getSquare() {
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    //получить видимую ширину
    public double getVisibleWeigth() {
        return 0;
    }

    @Override
    //получить видимую высоту
    public double getVisibleHeight() {
        return 0;
    }
}
