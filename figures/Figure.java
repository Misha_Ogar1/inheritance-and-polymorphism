package figures;

public interface Figure {

    //получить видимую ширину
    double getVisibleWeigth();

    //получить видимую высоту
    double getVisibleHeight();
}
