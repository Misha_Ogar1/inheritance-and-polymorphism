package figures;

public class Square extends Rectangle
implements Comparable<Square>
{

    //конструктор для задания ширины и длины
    public Square(double width, double height) {
        super(width, height);
        if (width != height) {
            System.out.println("The width doesn't equals the height!");
        }
    }

    //конструктор для задания ширины
    public Square(double width) {
        super(width, width);
    }

    //сеттер для задания шиниры и приравнения к ней высоты
    public void setWidth(double width) {
        setWidth(width);
        setHeight(width);
    }

    //сеттер для задания высоты и приравнения к ней шиниры
    public void setHeight(double height) {
        setHeight(height);
        setWidth(height);
    }

    @Override
    public int compareTo(Square square) {
        if (getWidth() > square.getWidth()) {
            return 1;
        }

        if (getWidth() < square.getWidth()) {
            return -1;
        }
        return 0;
    }
}
