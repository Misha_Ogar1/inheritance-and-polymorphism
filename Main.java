import figures.*;

public class Main {
    public static void main(String[] args) {

        //создание объекта square и задания ему параметров
        Square square = new Square(40);

        //выводим в консоль метод, выводящий площадь
        System.out.println("Площадь квардата равна: " + square.getSquare());

        //выводим в консоль метод, выводящий текущие дату и время
        System.out.println("Текущие дата и время: " + square.getCreationTime());

        //задаём параметры в нужный класс
        Figure2D figure = new Circle(3.4);
        Figure2D figure1 = new Rectangle(6, 7);
        Figure2D figure2 = new Square(4);

        //выводим площадь указанной фигуры
        System.out.println("Площадь окружности: " + figure.getSquare());
        System.out.println("Площадь прямоугольника: " + figure1.getSquare());
        System.out.println("Площадь квадрата: " + figure2.getSquare());
    }
}
